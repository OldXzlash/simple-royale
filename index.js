// Clash Royale Server

var net = require("net");
try {
	global.settings = require("./config.json");
} catch (err) {
	console.error("Make sure you have configured config.json, and that it exists.");
	process.exit(1);
}
require("jsbytes");
var libcrypto = require("./lib/crypto");
var libpacket = require("./lib/packet");
global.librw = require("./lib/rw");
global.libdb = require("./lib/db");

var router = require("./router");

var enummsgs = require("./enums/msgs");

global._clients = {};
require("./splash");
var server = net.createServer();
server.on("listening", () => {
	console.log("\t\tServer listening on port", settings.port);
});

server.on("connection", (connection) => {
	var cKey = connection.remoteAddress + ":" + connection.remotePort;
	console.log(`\t\tNew Client from ${cKey.slice(7)}`);
	_clients[cKey] = {};
	_clients[cKey].connection = connection;

	_clients[cKey].encrypto = modcrypto.init();
	_clients[cKey].decrypto = modcrypto.init();

	_clients[cKey].connection.on("data", (data) => {
	  
		var in_packets = libpacket.split(data);
		//console.log("raw input:", data.toString("hex"));
		in_packets.forEach((in_packet, index) => {
			//console.warn(`\t\t\tPacket received: ${in_packet.ID}`);
			//console.log("encrypted:",in_packet.payload.toString("hex"));

			_clients[cKey].decrypto.update(in_packet.payload);

			//console.log("decrypted:",in_packet.payload.toString("hex"));
			in_packet.client = _clients[cKey];
			in_packet.cKey = cKey;
			in_packet.reader = new librw.Reader(in_packet.payload);
			
			in_packet.writer = new librw.Writer();

			var replies = router.handle(in_packet);
			if(!replies){return;}
			replies.forEach((reply) => { //reply: librw.Writer
				if(!reply){return;}
				_clients[cKey].encrypto.update(reply.Buf);
			
				var out_payload = libpacket.join(reply.ID, reply.Buf);
				
				_clients[cKey].connection.write(out_payload);
				console.log(`Packet was sent: ${msgs.server[reply.ID].green} (${reply.ID.toString().green})`);
			});

			delete in_packet
		});
	});

	_clients[cKey].connection.on("close", () => {
		console.log(`\t\t\tClosed Client: ${cKey.slice(7)}`);
		delete _clients[cKey];
	});

});

server.listen(settings.port);