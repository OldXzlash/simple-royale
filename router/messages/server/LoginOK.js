function LoginOK (opts) {
  var o = new librw.Writer()
  var ogetter = new optionGetter(opts, {
    id: 0,
    passToken: 'hello',
    gameCenterID: '',
    facebookID: '',
    serverMajorVersion: 0,
    serverBuild: 0,
    contentVersion: 0,
    environment: '',
    sessionCount: 0,
    playTimeSeconds: 0,
    accountAgeDays: 0,
    facebookAppID: '',
    serverTime: String(Math.round(new Date().getTime() / 1000)),
    accountCreationTime: String(Math.round(new Date().getTime() / 1000)),

    googleID: '',

    country: 'CA',
    city: 'Toronto',
    state: 'ON',

    contentURL: '',
    eventAssetsURL: ''

  })

  o.setID(20104)
		.writeLong(ogetter.get('id'))
		.writeLong(ogetter.get('id'))
		.writeString(ogetter.get('passToken'))
		.writeString(ogetter.get('gameCenterID'))
		.writeString(ogetter.get('facebookID'))
		.writeVInt(ogetter.get('serverMajorVersion'))
		.writeVInt(ogetter.get('serverBuild'))
		.writeVInt(ogetter.get('serverBuild'))
		.writeVInt(ogetter.get('contentVersion'))
		.writeString(ogetter.get('environment'))
		.writeVInt(ogetter.get('sessionCount'))
		.writeVInt(ogetter.get('playTimeSeconds'))
		.writeVInt(ogetter.get('accountAgeDays'))
		.writeString(ogetter.get('facebookAppID'))
		.writeString(ogetter.get('serverTime'))
		.writeString(ogetter.get('accountCreationTime'))
		.writeVInt(0)
		.writeString(ogetter.get('googleID'))
		.writeString('')
		.writeString('')
		.writeString(ogetter.get('country'))
		.writeString(ogetter.get('city'))
		.writeString(ogetter.get('state'))
		/* .writeVInt(1)
		.writeVInt(510333)
		.writeVInt(44667)
		.writeVInt(2)
		.writeString(ogetter.get("contentURL"))
		.writeString(ogetter.get("eventAssetsURL"))
		.writeVInt(1)
		.writeString("") //eventassetsURL
		.writeByte(0)
		*/
		.writeHex('01afc333cbfd1802')
		.writeHex(`00000026
    
68747470733a2f2f67616d652d6173736574732e636c617368726f79616c656170702e636f6d
    
    00000051
    68747470733a2f2f39396661663165333535633734396139613034392d32613633663434333663393637616137643335353036316264306439323461312e73736c2e6366312e7261636b63646e2e636f6d01
    
    00000024
    68747470733a2f2f6576656e742d6173736574732e636c617368726f79616c652e636f6d`)

  return o
}

module.exports.ok = (in_packet, opts) => {
  var o = LoginOK(opts)
  return o
}
