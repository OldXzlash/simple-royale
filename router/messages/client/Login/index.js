var LoginFailed = require('../../server/LoginFailed')
var LoginOK = require('../../server/LoginOK')
var OwnHomeData = require('../../server/OwnHomeData')
var shortcards = require('../../../../enums/shortcards')

module.exports.main = (in_packet) => {
  var r = in_packet.reader

  var accountID = r.readLong('accountID')
  var passToken = r.readString('passToken')

  _clients[in_packet.cKey].data = libdb.getPlayer(accountID, passToken)
  libdb.savePlayer(_clients[in_packet.cKey].data)

  switch (_clients[in_packet.cKey].data.state) {
    case 'maintenance':
      return [ LoginFailed.maintenance(in_packet) ]
      break
    case 'ban':
      return [ LoginFailed.banned(in_packet) ]
      break
    case 'ok':
			// Get user from DB here. Store info in opts.

      var hopts = in_packet.client.data.HomeData
      var oopts = in_packet.client.data

      return [ LoginOK.ok(in_packet, oopts), OwnHomeData.OHD(in_packet, hopts)]
      break
    default:
      return [ LoginOK.ok(in_packet, oopts), OwnHomeData.OHD(in_packet, hopts)]
      break
  }
}
module.exports.GoHome = (in_packet) => {
  var opts = in_packet.client.data.HomeData
  return [ OwnHomeData.OHD(in_packet, opts) ]
}
