var shortcards = require('../../../enums/shortcards')
var cardarray = require('../../../enums/cardarray')

function random (min, max) {
  var a = Math.floor(Math.random() * (max - min + 1) + min)

  return a
}

function main (reader, incmd) {
  reader.getBytes(12)
  var ChestID = reader.readVInt()

  console.log(reader.orig)
  var pull = {
    'cards': [],
    'gems': 1
  }

  var CCount = 1000

  for (var i = 0; i < CCount; i++) {
    pull.cards.push({
      name: cardarray[random(0, 1 - 1)],
      count: random(1, 100)
    })
  }

  pull.gems = random(1, 10000000)
  pull.gold = random(1, 10000000)

  console.log(JSON.stringify(pull))
  var o = new librw.Writer()

// <Buffer 92 03 - 92 03 - 9e 05 b9 d7 b4 fa 0e - 13 - a1 03 - ff ff ff ff> smc
// <Buffer 89 04 - 89 04 - 9e 05 b9 d7 b4 fa 0e - 13 - a1 03 - ff ff ff ff> smc
// <Buffer ac 01 - ac 01 - 9e 05 b9 d7 b4 fa 0e - 13 - a4 03 - ff ff ff ff> mc

  o.writeByte(1)
		.writeBool(false) // isDraft
		.writeVInt(pull.cards.length)

  for (var index = 0; index < pull.cards.length; index++) {
    var card = pull.cards[index]

    o.writeVInt(shortcards[card.name][0])
			.writeByte(0)
			.writeVInt(1)
			.writeVInt(card.count)
			.writeByte(0)
			.writeByte(0)
			.writeByte(0) // Card Order
  };

  o.writeByte(127)
		.writeVInt(pull.gold)
		.writeVInt(pull.gems)
		.writeVInt(ChestID)
		.writeByte(4)
		.writeByte(14)
		.writeHex('7f7f')
		.writeByte(4)
		.writeByte(10)

  return {
    'ID': 210,
    'payload': o.Buf
  }
}

module.exports = {
  'main': main
}
