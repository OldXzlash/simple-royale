var cardlookup = require('../../../enums/cardsbyhilo')

module.exports.main = (reader) => {
  var cmpa = reader.getBytes(2)
  if (reader.rem.slice(0, 2).toString('hex') == cmpa.toString('hex')) {
    reader.getBytes(2)
  }
  reader.getBytes(10)
  reader.readByte('Count')
  reader.readByte('hi')
  reader.readByte('lo')
  console.log(`${reader.JSON.Count}x "${cardlookup[reader.JSON.hi][reader.JSON.lo]}" card${(reader.JSON.Count == 1) ? ' was' : 's were'} purchased from the shop.`.cyan)
}
