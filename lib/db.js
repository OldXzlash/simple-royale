const fs = require('fs')

module.exports.getPlayer = (accountID, passToken) => {
  if (accountID == 0) {
    console.log('New Player!')
			// DISABLED FOR DEBUGGING: var plyr = {"id":Date.now(),"passToken":require("crypto").randomBytes(20).toString("hex"),"HomeData":{"Name":"DATABASE","Clan":{"Name":"IT WORKS"}},"state":"ok"};
    var plyr = {'id': 1505245959673, 'passToken': require('crypto').randomBytes(20).toString('hex'), 'HomeData': {'Name': 'DATABASE', 'Clan': {'Name': 'IT WORKS'}}, 'state': 'ok'}
    return plyr
  }

  try {
    var player = require(`../data/players/${accountID}.json`)
  } catch (e) {
    return {'id': 0, 'passToken': 'bad', 'state': 'ban'}
  }

  if (passToken == player.passToken) {
    console.log('yep')
    return player
  } else {
    console.log('NOPE')
    return {}
  }
}

module.exports.savePlayer = (player) => {
  fs.writeFile(`data/players/${player.id}.json`, JSON.stringify(player), (r, e) => { console.log(`saved`, player.id) })
}
