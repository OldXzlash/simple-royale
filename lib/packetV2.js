class Packet {
  constructor (arg) {
    if (Buffer.isBuffer(arg)) {
			// Split Packet
      this.packets = []

      var keepGoing = true
      var offset = 0
      var raw = arg
      while (keepGoing) {
        var _out = {}
        _out.ID = int.from_bytes(raw.slice(offset, offset + 2), 'big')
        offset += 2
        _out.sentlength = int.from_bytes(raw.slice(offset, offset + 3), 'big')
        offset += 3
        _out.sentversion = int.from_bytes(raw.slice(offset, offset + 2), 'big')
        offset += 2

        _out.payload = raw.slice(offset, _out.sentlength + offset)
        offset += _out.sentlength

        packets.push(_out)
        if (raw.length - offset < 1) {
          keepGoing = false
        }
      }
    }		 else if (typeof (arg) === 'Object') {
			// Join Packet
      var {id, payload} = arg
      this.out = Buffer.concat([
        id.to_bytes(2, 'big'),
        payload.length.to_bytes(3, 'big'),
        int(3).to_bytes(2, 'big'),
        payload
      ])
    }
  }
}
