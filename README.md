# simple-royale

This is a deprecated attempt at a Clash Royale Private Server.

> You can get the last working commit [here](https://gitlab.com/Xzlash/simple-royale/commit/df40a3c207f45edc775b9a6edfbb40f1ad9291db) (**Only supports Clash Royale version 1.9.x**).
> We are currently working on a better (and cleaner) version of this program to use. Feel free to __[Join our Discord server](https://discord.me/rlg)__ for more info and to play on this new version.

### Installation
First go ahead and [Download the program](https://gitlab.com/Xzlash/simple-royale/repository/df40a3c207f45edc775b9a6edfbb40f1ad9291db/archive.zip), and extract the files. We now assume you are using a Debian-based Linux system such as [Ubuntu](https://ubuntu.org).

###### Install Node.js
This program requires Node.js v8 or newer. Some features may not work correctly or at all if you use an older version.
You can download Node.js [here for Windows](https://nodejs.org/en/download/current/) or [here for Linux](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)

###### Initialize the program (install dependencies)
Open a terminal and make sure you are in the directory you extracted from the `.zip` file.

Run this command (as sudo):

`# npm install`

Wait a few minutes for the packages to install, then, if there are no errors, continue to the next step (if there are errors, you can get support in our Discord server (see above)).


###### Running
You are now done setting up your server! Now to connect your game to it, you will need to do some stuff! There is no support for iOS, only Android. You will need a modified APK to connect to the server, and you will also need a DNS record that is **exactly 23 characters long** that points to your server. For information about how to modify the APK, again, join our Discord server.

# Contributing
This repository is archived, and is only here for reference, we do not recommend using it, as it is not coded very well.

If you would like to help us with our current and future projects, check out these two links:

 - [Developer Job Application](https://docs.google.com/forms/d/e/1FAIpQLScK0FRZV7klr6SxUwL201VHASdwlnu-L6WiSsSRHdN6SEz7Og/viewform)
 - [Discord](https://discord.me/rlg)